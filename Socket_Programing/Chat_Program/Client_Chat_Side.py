import socket
import threading
import socket
import time


client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_ip = socket.gethostbyname(socket.gethostname())
port = 8001

socket_address = (client_ip, port)

client_socket.bind(socket_address)
lock = threading.Lock()
print("Start sending messages")

def receive_server(name , sock):
    """Description.
       receive function receive the message from server
    """
    while True:
        try:
           lock.acquire()
           while True:
                data, address= client_socket.recvfrom(1024)
                print("Message from server: " +data.decode('utf-8'))
        except Exception as e:
            print(e)
        finally:
            lock.release()



def send_server(name, sock):
    """Description.
            send function sends the message from client to client

            Parameters
            ----------
            @param name
                Description of arg1
                name of the server if need to mention
            @param  sock
                 socket address of server
    """

    try:
        message = input()
        while message != "quit":
            client_socket.sendto(message.encode('utf-8'), (client_ip, 8002))
            message = input()
            time.sleep(2)
        print("closing connection")
        client_socket.close()
    finally:
        client_socket.close()

send_thread = threading.Thread(target=send_server, args=("", client_socket))
receive_thread = threading.Thread(target=receive_server, args=("", client_socket))
send_thread.start()
receive_thread.start()
receive_thread.join(2)


