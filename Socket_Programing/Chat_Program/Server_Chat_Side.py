import socket
import threading
import time


server_ip=socket.gethostbyname(socket.gethostname())
port=8002
clients=[]
server_socket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind((server_ip, port))
lock1= threading.Lock()
print("Start sending messages")

def receive_client(name, sock):
    """Description.
          receive_client function receive the message from client
    """

    while True:
        try:
           lock1.acquire()
           while True:
                data, address= server_socket.recvfrom(1024)
                print("Message from client: " +data.decode('utf-8'))
        except Exception as e:
            print(e)
        finally:
            lock1.release()



def send_client(name, sock):
    """Description.
          send_client function sends  the message to client
    """

    message_server = input()
    while message_server!="quit":
            server_socket.sendto(message_server.encode('utf-8'),(server_ip,8001))
            message_server = input()
            time.sleep(2)
    print("closing connection")
    server_socket.close()

receive_thread=threading.Thread(target=send_client ,args=("", server_socket))
send_thread=threading.Thread(target=receive_client ,args=("", server_socket))
receive_thread.start()
send_thread.start()
send_thread.join(2)
