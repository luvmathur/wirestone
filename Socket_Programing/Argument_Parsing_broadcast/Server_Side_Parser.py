import argparse
import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send_message():
    """
    Description.
     send message sends data or message to client
    """
    # Description of the client_parser1.py
    parser = argparse.ArgumentParser(description="Enter port number to send message to client")
    # arguments are passed at command prompt
    parser.add_argument('ip', help="ip address")
    parser.add_argument('port', help="server port number", type=int)
    parser.add_argument('port1', help="port number", type=int)
    parser.add_argument('port2', help="port number", type=int)
    args = parser.parse_args()
    server_socket.bind((args.ip, args.port))
    print("start sending messages")
    message = input()
    while message != "quit":
        try:
            server_socket.sendto(message.encode('utf-8'), (args.ip, args.port1))
            server_socket.sendto(message.encode('utf-8'), (args.ip, args.port2))
            message = input()
        except Exception as e:
            print(e)

    print("connection is closed")
    server_socket.close()

send_message()