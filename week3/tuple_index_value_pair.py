def tuple_index_value(my_list):
    """
               Description.
               This function uses input list to map its index value pair

               Parameters
               ----------
               @param  my_list
                   sample list is passed to a function


                     Returns
               -------
               index value pair is returned

    """
    list1 = []
    for val in enumerate(my_list):
        list1.append(val)
    print(list1)


if __name__ == "__main__":
    tuple_index_value([2, 4, 6, 9, 1, 3])