import urllib
import soup as soup
from bs4 import BeautifulSoup
import requests
import lxml
import os
import request
import parser
import numpy
from matplotlib import pylab


class dataextract:

    def __init__(self):
        """
        Description.
        This constructor initialize the url and get the content of it using beautiful aoup
        """
        self.url = "https://www.tripadvisor.in/"
        self.response = requests.get(self.url)
        status = self.response.status_code
        self.content = self.response.content
        self.soup = BeautifulSoup(self.content, 'lxml')

    def get_home_page_data(self):
        """
        Description.
        get_home_page_data returns the content of home page of tripadvisor
        """

        proper_format = self.soup.prettify()
        print(proper_format)


    def hotel_info(self):
        """
        Description.
        hotel_info returns the list of hotels registered to tripadvisor
        """

        self.soup = BeautifulSoup(self.content, 'lxml')
        hotels_city_wise = self.soup.findAll('li',class_="item")
        print(hotels_city_wise)

        for hotels in hotels_city_wise:
            print(hotels.text)


    def state_hotel(self):
        """
        Description.
        state_hotel returns the list of hotels in rajasthan registered to tripadvisor
        """
        state_hotels = self.soup.find('a', href="/Hotels-g297665-Rajasthan-Hotels.html")
        state_hotel_link = requests.get(self.url+"/Hotels-g297665-Rajasthan-Hotels.html")
        soup_raj_hotel = BeautifulSoup(state_hotel_link.content,"lxml")
        state_hotel_pretify = soup_raj_hotel.prettify()
        list_hotels_raj=soup_raj_hotel.findAll('a',class_="property_title")
        for self.hotel_raj in list_hotels_raj:
            print(self.hotel_raj.text)


    def hotel_price(self):
        """
        Description.
        hotel_price returns the list of hotels in jaipur with their price
        """
        url_jpr = "https://www.tripadvisor.in/Hotels-g304555-Jaipur_Jaipur_District_Rajasthan-Hotels.html"
        response_url=requests.get(url_jpr,'lxml')
        soup_jpr=BeautifulSoup(response_url.content,'lxml')
        links=soup_jpr.findAll('a',class_="property_title")
        price_data=soup_jpr.findAll('div',class_="price")
        list1=[]
        for link in links:
            for price in price_data:
                print(link.text,price.text)
                continue

if __name__=="__main__":
    tripadvisor_obj=dataextract()
    #tripadvisor_obj.get_home_page_data()
    #tripadvisor_obj.hotel_info()
    #tripadvisor_obj.state_hotel()
    tripadvisor_obj.hotel_price()
