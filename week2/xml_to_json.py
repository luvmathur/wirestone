import xml
from xml.dom.minidom import parse
import os


class xmltojson:
    """  Xm lRead is designed to read xml format"""

    def __init__(self):

        self.read_from = "C:\\Users\\luv.mathur\\Desktop\\sample.xml"
        self.target_directory = "F:\\xml_to_json"
        try:
            os.mkdir(self.target_directory)
            print("Try completed")
        except:
            print("Directory exist so Parsing has begun ")

    def xml_dom_parsing(self):

        """
            Description.
            xml_dom_parsing parse the file from input file directory
            Parameters
            ----------
            no parameter is passed

            Returns
            -----------
        """

        DOMTree = xml.dom.minidom.parse(self.read_from)
        collection = DOMTree.documentElement
        self.student_data = collection.getElementsByTagName("student")

    def read_data(self):
        """
                    Description.
                    Reads the xml format and convert into dictionary having key:value pair
                    Parameters
                    ----------
                    no parameter is passed

                    Returns
                    -----------
                """

        for data in self.student_data:
            if data.hasAttribute('name'):
                tuple = ('degree', 'stream', 'cgpa', 'city')
                degree = data.getElementsByTagName('degree')[0]
                stream = data.getElementsByTagName('stream')[0]
                cgpa = data.getElementsByTagName('cgpa')[0]
                city = data.getElementsByTagName('city')[0]
                self.dict_xml = {'degree': degree.childNodes[0].data, 'stream': stream.childNodes[0].data,
                                 'cgpa': cgpa.childNodes[0].data, 'city': city.childNodes[0].data}

        self.person = data.getAttribute('name')

    def write_data(self):
        """
                            Description.
                            Write the json format into target file specified
                            Parameters
                            ----------
                            no parameter is passed

                            Returns
                            -----------
                        """
        os.chdir(self.target_directory)
        fobj = open("xml_to_json.txt", "w+")
        fobj.write(self.person+"\n")
        fobj.write(""+str(self.dict_xml))
        fobj.close()
        print("File has been written at:", self.target_directory+"\\xml_to_json.txt")

xml_json = xmltojson()
xml_json.xml_dom_parsing()
xml_json.read_data()
xml_json.write_data()