
def prime_value_list():
    """
     Description.
      prime_value_list function determine number of prime number less than or equal to given sample input

     Parameters
     ----------
      no parameter is passed instead value is passed at console

     Returns
     -------
     Returns a list containing all prime number less than given input

     """

    print("Enter number to get prime number upto that range", "\n")
    number = int(input())
    prime_list = [num for num in range(2, number+1) if all(num % y != 0 for y in range(2, num))]
    print(prime_list)


prime_value_list()







