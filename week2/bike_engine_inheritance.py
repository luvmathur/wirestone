class Engine1:
    def engine1_characteristics(self,feature,power):
        self.feature = feature
        self.power = power

    def engine1_display(self):
        print("Feature",self.feature,self.power)

class Engine2:
    def engine2_characteristics(self, feature, power):
        self.feature = feature
        self.power = power

    def engine2_display(self):
        print(self.feature, self.power)

class Engine3:
    def engine_characteristics(self, feature, power):
        self.feature = feature
        self.power = power

    def engine3_display(self):
        print(self.feature,self.power)

class Engine:

    def __init__(self, type_vehicle=""):
        self.type_vehicle = type_vehicle

    def bike_engine_parameters(self, message, *characteristics):
        self.message = message
        self.characteristics = characteristics

    def engine_display(self):
        print(self.message, self.characteristics)


class Bike1(Engine,Engine1,Engine2,Engine3):


    def bike_characteristics(self, name, *about_bike):
        self.name = name
        self.about_bike = about_bike

    def display_bike_characteristics(self):
        print(self.name)
        print("Bike Features", self.about_bike, end='      ')


if __name__ == "__main__":
    print("Calling from main Function", "\n")
    bike1 = Bike1()
    bike1.bike_characteristics("Name: Hero HF Deluxe", "Blue", "97CC", "Radial Engine", "Rs.38460")
    bike1.bike_engine_parameters("Engine characteristics", "Single cylinder", "V Type")
    bike1.engine1_characteristics("petrol", "70 BHP")
    bike1.display_bike_characteristics()
    bike1.engine_display()
    bike1.engine1_display()
    bike2 = Bike1()
    bike2.bike_characteristics("\n""Name: Yamaha Saluto RX", "Red", "110CC", "Turbine Engine", "Rs.48021")
    bike2.bike_engine_parameters("Engine characteristics", "Double cylinder", "Inline V-Type")
    bike2.engine2_characteristics("Petrol", "110 BHP")
    bike2.display_bike_characteristics()
    bike2.engine_display()
    bike2.engine2_display()
    bike3 = Bike1()
    bike3.bike_characteristics("\n""Name: Bajaj Pulsar", "Black", "125CC", "W Type Engine", "Rs.85340")
    bike3.bike_engine_parameters("Engine characteristics", "Six cylinder", "Wankel")
    bike3.engine2_characteristics("Petrol", "230 BHP")
    bike3.display_bike_characteristics()
    bike3.engine_display()
    bike3.engine2_display()


else:
    print("You cannot call values from importing module")


