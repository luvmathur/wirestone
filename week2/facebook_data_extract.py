import facebook
import requests
import pprint

class Fbdataextract:

    """ This class is designed to extract facebook information"""

    def __init__(self):
        self.credentials = {'page_id': '1735410499832652',
                             'access_token': 'EAACEdEose0cBAGJDVUr3nCPHia4o0u6yaodG0Q3Kb110yLKGZAN7miKYxLyBPYAPRde0Ioo3RQ1L0eApoJurGfQOwmPpqIlONRXrgzapF1gVrgStInC7E6F6J0EKCZB6uAm4TzkSazsItpvGUGkGBLAg1dxYmZCzGTgX3SoJEGYj7SAwHbqGUZB250X8QfAZD'}
        api = self.fb_post()
        print("Enter message you want to post")
        message = input()
        status = api.put_wall_post(message)
        print("Message posted")

    def fb_post(self):
        """
            Description.
             function post the status on sample page on facebook

        """

        graph = facebook.GraphAPI(self.credentials['access_token'])
        response = graph.get_object('me/accounts')
        page_access_token = None
        for page in response['data']:
                if page['id'] == self.credentials['page_id']:
                    page_access_token = page['access_token']
                    graph = facebook.GraphAPI(page_access_token)
                    return graph

    def get_friends_count(self):
        """
        Description.
        get_friends returns the number of facebook friends user is having

        """

        graph = facebook.GraphAPI(self.credentials['access_token'])
        friends = graph.get_object("me/friends")
        print("Total Friends: ", friends["summary"]["total_count"])

    def get_profile(self):
        """
        Description.
        get_profile returns the peson profile with name and id
        """

        graph = facebook.GraphAPI(self.credentials['access_token'])
        myself = graph.get_object("me")
        print(myself)

    def my_post(self):
        """
        Description.
        my_post returns all post by a user on facebook
        """

        graph = facebook.GraphAPI(self.credentials['access_token'])
        profile = graph.get_object('me')
        posts = graph.get_connections(profile['id'], 'posts')
        pprint.pprint(posts)

if __name__ == "__main__":
    fb = Fbdataextract()
    fb.get_profile()
    fb.get_friends_count()
    fb.my_post()