#include<stdio.h>
#include<stdlib.h>
# define null '\0'

typedef struct queue{
	struct queue *next;
	int initial_power;
	int position;
}*queue;
struct queue *head='\0';
struct queue *p='\0';

void push(int power,int position){

	struct queue *tmp=malloc(sizeof(queue));
	tmp->next=null;
	if(power<0){
		power = 0;
	}
	tmp->initial_power=power;
	tmp->position=position;

	if(head==null){
		head=tmp;
		p=head;
	}
	else{
		p->next=tmp;
		p=p->next;
	}
}

struct queue *pop(){
	if(head==null){
		return;
	}
	else{
		struct queue *tmp=head;
		head=head->next;
		tmp->next=null;
		return tmp;
	}
}

int find_max_position(struct queue *head,int X,int N){
	int i=0,max=-99999,position=0;
	struct queue *tmp = head;
	for(i=0;i<X;i++){
		if(tmp){
			if(tmp->initial_power>max){
				max = tmp->initial_power;
				position = tmp->position;
			}
			tmp=tmp->next;
		}
	}
	return position;
}


int main(){
	
	int N,X,i,j;
	scanf("%d%d",&N,&X);
	int power=0;
	for(i=1;i<=N;i++){
		scanf("%d",&power);
		push(power,i);
	}
	for(i=0;i<X;i++){ //number of iterations
		int total_pop_needed=X;
		if(total_pop_needed>N){
			total_pop_needed=N;
		}
		int max_position = find_max_position(head,total_pop_needed,N);
		printf("%d ",max_position);
		for(j=0;j<total_pop_needed;j++){ //number of pop
			struct queue *tmp = pop();
			if(tmp->position != max_position){
				push(tmp->initial_power-1,tmp->position);
			}
		}
		N--;
	}	
	return 0;
}