my_list = [22, 67, 89, 0, 0, 3, 2, 1]

print("unsorted list:" + str(my_list))


def sortlist():
    """
           Description.
           This function sortlist sort the sample list in increasing order

           Parameters
           ----------
           @ param list  my_list
               sample list is passed to a sortlist function


                 Returns
           -------
           sorted list is output

     """
i = 0
j = 0
for i in range(0, len(my_list)-1):
    for j in range(i+1, len(my_list)):
        if my_list[i] < my_list[j]:
            j = j+1
        else:
            my_list[i], my_list[j] = my_list[j], my_list[i]
            j = i+1
i = i+1
sortlist()
print("sorted list:" + str(my_list))

dict1 = {'age': 12, 'weight': 21, 'height': 5}
dict1_list = list(dict1.values())

print("\n\nunsorted values of dictionary:" + str(dict1_list))


def sort_dictionary():
    """
               Description.
               This function sort_dictionary sort the sample dictionary values in increasing order

               Parameters
               ----------
               no parameters are passed



                     Returns
               -------
               sorted dictionary values is output

         """


k = 0
l = 0
for k in range(0, len(dict1)-1):
    for l in range(k+1, len(dict1)):
        if dict1_list[k] < dict1_list[l]:
            l = l+1
    else:
        dict1_list[k], dict1_list[l] = dict1_list[l], dict1_list[k]
    l = k+1
k = k+1

sort_dictionary()
print("sorted values of dictionary:" + str(dict1_list))