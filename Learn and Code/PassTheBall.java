import java.util.Scanner;

class PassTheBall{

	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int test_cases;
		test_cases=sc.nextInt();
		for(int k=0;k<test_cases;k++){
			int number_of_passes=sc.nextInt();
			int pid=sc.nextInt();
			int current_ball_holder=pid,ball_passed_by=0;
			for(int i=0;i<number_of_passes;i++){
				String pass_type="";
				pass_type=sc.next();
				if(pass_type.equals("P")){
					pid=sc.nextInt();
					ball_passed_by=current_ball_holder;
					current_ball_holder=pid;
				}
				else{
					int tmp=current_ball_holder;
					current_ball_holder=ball_passed_by;
					ball_passed_by=tmp;
				}
			}
			System.out.println("Player "+current_ball_holder);
		}
		
	}
}