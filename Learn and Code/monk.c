#include<stdio.h>
#include<stdlib.h>
# define null '\0'

typedef struct queue{
	struct queue *next;
	int initial_power;
	int position;
}*queue;
struct queue *head='\0';
struct queue *p='\0';

void push(int power,int position){

	struct queue *tmp=malloc(sizeof(queue));
	tmp->next=null;
	if(power<0){
		power = 0;
	}
	tmp->initial_power=power;
	tmp->position=position;

	if(head==null){
		head=tmp;
		p=head;
	}
	else{
		p->next=tmp;
		p=p->next;
	}
}

struct queue *pop(){
	if(head==null){
		return;
	}
	else{
		struct queue *tmp=head;
		head=head->next;
		tmp->next=null;
		return tmp;
	}
}

int find_max_position(struct queue *head,int SpiderSelected,int NumberOfSpider){
	int SpiderCounter=0,max=-99999,position=0;
	struct queue *tmp = head;
	for(SpiderCounter=0;SpiderCounter<SpiderSelected;SpiderCounter++){
		if(tmp){
			if(tmp->initial_power>max){
				max = tmp->initial_power;
				position = tmp->position;
			}
			tmp=tmp->next;
		}
	}
	return position;
}


int main(){
	
	int NumberOfSpider,SpiderSelected,SpiderCounter,SpiderPopCounter;
	scanf("%d%d",&NumberOfSpider,&SpiderSelected);
	int power=0;
	for(SpiderCounter=1;SpiderCounter<=NumberOfSpider;SpiderCounter++){
		scanf("%d",&power);
		push(power,SpiderCounter);
	}
	for(SpiderCounter=0;SpiderCounter<SpiderSelected;SpiderCounter++){ //number of iterations
		int total_pop_needed=SpiderSelected;
		if(total_pop_needed>NumberOfSpider){
			total_pop_needed=NumberOfSpider;
		}
		int max_position = find_max_position(head,total_pop_needed,NumberOfSpider);
		printf("%d ",max_position);
		for(SpiderPopCounter=0;SpiderPopCounter<total_pop_needed;SpiderPopCounter++){ //number of pop
			struct queue *tmp = pop();
			if(tmp->position != max_position){
				push(tmp->initial_power-1,tmp->position);
			}
		}
		NumberOfSpider--;
	}	
	return 0;
}