from Quiz.config  import db
from flask import Flask, render_template, jsonify, request, json, make_response
from flask_restful import reqparse, abort, Api, Resource
from Quiz.v1.api import questions, users


def get_flask_obj():
    app = Flask(__name__)
    return app
#app.run(debug=True)
