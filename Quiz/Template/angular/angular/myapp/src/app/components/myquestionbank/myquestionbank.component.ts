import { Component, OnInit,EventEmitter, OnChanges } from '@angular/core';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';
import {AuthenticationService} from '../../services/authentication.service';


@Component({
  selector: 'app-myquestionbank',
  templateUrl: './myquestionbank.component.html',
  styleUrls: ['./myquestionbank.component.css']
})
export class MyquestionbankComponent implements OnInit {
public languages = new Array();


public questions;


constructor(private router:Router,
  private _cookieService:CookieService, private _headerservice: HeaderService,
  private _authenticationService: AuthenticationService) {
  }



getlanguages(){
this._authenticationService.getinformation().subscribe(data=>{
for (var value in data.Languages){
console.log(data.Languages[value]);
this.languages.push(data.Languages[value].language);
}
},

err => {
     alert('error');
}
);
}


getquestions(value:any){
this._authenticationService.getquestions(value).subscribe(data=>{
var num: number=0;
this.questions = data.questions;
},
err => {
     alert('error');
}
);
}

update(value:any){
var json ={"question":document.getElementById('q').innerHTML,"choice1":document.getElementById('c1').innerHTML,
"choice2":document.getElementById('c2').innerHTML,
"choice3":document.getElementById('c3').innerHTML,
"choice4":document.getElementById('c4').innerHTML,
"answer":document.getElementById('ans').innerHTML
}

this._authenticationService.editquestion(value, json).subscribe(data=>{
alert('updated');
},
err => {
     alert('not updated');
}
);
}



deletequestion(value:any){
this._authenticationService.deletequestion(value).subscribe(data=>{
alert('deleted');
},
err => {
     alert('not deleted');
}
);
}

ngOnInit() {
this.getlanguages();
}

}


