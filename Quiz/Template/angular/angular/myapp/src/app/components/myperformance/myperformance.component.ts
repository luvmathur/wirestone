import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router, ActivatedRoute} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';

@Component({
  selector: 'app-myperformance',
  templateUrl: './myperformance.component.html',
  styleUrls: ['./myperformance.component.css']
})
export class MyperformanceComponent implements OnInit {

public questions = new Array();
public answers = new Array();
public correctquestions = new Array();

 constructor(private _authenticationService: AuthenticationService,
private router: Router,  private _cookieService:CookieService,
private _headerservice:HeaderService, private _activatedroute: ActivatedRoute
){}

getquestions(){
var topic_id = this._activatedroute.snapshot.params['id'];
this._authenticationService.gettopicquestions(topic_id).subscribe(data=>{
for (var value in data){
this.questions.push(data[value]);
}
},
err => {
     alert(err);
}
);
}

getresult(){
var topic_id = this._activatedroute.snapshot.params['id'];
this._authenticationService.getuserperformance(topic_id).subscribe(data=>{
for (var value in data){
this.answers.push(data[value]);
}
this.correctquestions.push(data[0].correct);
},
err => {
     alert(err);
}
);
}

close(){
var topic_id = this._activatedroute.snapshot.params['id'];
this._authenticationService.deletetestentry(topic_id).subscribe(data=>{
this.router.navigate(['userdashboard']);
},
err => {
     alert(err);
}
);
}



  ngOnInit() {
this.getquestions();
this.getresult();
console.log(this._activatedroute.snapshot.params['id']);

  }

}
