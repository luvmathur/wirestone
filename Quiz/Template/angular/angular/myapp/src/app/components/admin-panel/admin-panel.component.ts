import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

public topics = new Array();
public languages = new Array();


constructor(private router:Router,
  private _cookieService:CookieService, private _headerservice: HeaderService,
  private _authenticationService: AuthenticationService) {
  }


getlanguages(){
this._authenticationService.getinformation().subscribe(data=>{
for (var value in data.Languages){
console.log(data.Languages[value]);
this.languages.push(data.Languages[value].language);
}
},

err => {
     alert('error');
}
);
}

gettopics(){
this._authenticationService.gettopics().subscribe(data=>{
for (var value in data.topics){
console.log(data.topics[value]);
this.topics.push(data.topics[value]);
}
},

err => {
     alert('error');
}
);

}


questions(value:any){
console.log(value);
this._authenticationService.addquestion(value).subscribe(data => {
      alert('question added successfully');

err => {
     alert('Question not added');
}
});
}

addlanguage(value:any){
console.log(value);
this._authenticationService.addnewlanguage(value).subscribe(data => {
      alert('Language added successfully');

err => {
     alert('Language not added');
}
});
}

addtopic(value:any){
this._authenticationService.addnewtopic(value).subscribe(data => {
      alert('Topic added successfully');

err => {
     alert('Topic not added');
}
});

}


ngOnInit() {
this.gettopics();
this.getlanguages();
}
}
