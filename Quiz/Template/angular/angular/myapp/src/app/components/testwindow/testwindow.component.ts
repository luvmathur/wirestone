import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router, ActivatedRoute} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';

@Component({
  selector: 'app-testwindow',
  templateUrl: './testwindow.component.html',
  styleUrls: ['./testwindow.component.css'],
})

export class TestwindowComponent implements OnInit {

public questiondata = new Array();

 constructor(private _authenticationService: AuthenticationService,
private _router: Router,  private _cookieService:CookieService,
private _headerservice:HeaderService, private _activatedroute: ActivatedRoute
){}

getquestions(){
var topic_id = this._activatedroute.snapshot.params['id'];
this._authenticationService.gettopicquestions(topic_id).subscribe(data=>{
for(var value in data){
this.questiondata.push(data[value]);
}
},
err => {
     alert('error');
}
);

}

setanswer(question, response){
var test_id = this._activatedroute.snapshot.params['id'];
var userresponse = {"topic_id":test_id,"response":response};
this._authenticationService.setuserresponse(question, userresponse).subscribe(data=>{
},
err => {
     alert(err);
}
);
}

getresult(){
var test_id = this._activatedroute.snapshot.params['id'];
this._authenticationService.getuserperformance(test_id).subscribe(data=>{
this._router.navigate(['myperformance', test_id]);
},
err => {
     alert(err);
}
);

}



  ngOnInit() {
this.getquestions();
console.log(this._activatedroute.snapshot.params['id']);

 }

}
