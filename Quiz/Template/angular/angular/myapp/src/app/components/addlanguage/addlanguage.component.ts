import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';
import {AuthenticationService} from '../../services/authentication.service';



@Component({
  selector: 'app-addlanguage',
  templateUrl: './addlanguage.component.html',
  styleUrls: ['./addlanguage.component.css']
})
export class AddlanguageComponent implements OnInit {

   constructor(private router:Router,
  private _cookieService:CookieService, private _headerservice: HeaderService,
  private _authenticationService: AuthenticationService) {
  }


addlanguage(value:any){
console.log(value);
this._authenticationService.addnewlanguage(value).subscribe(data => {
      alert('Language added successfully');

err => {
     alert('Language not added');
}
});
}





  ngOnInit() {
  }

}
