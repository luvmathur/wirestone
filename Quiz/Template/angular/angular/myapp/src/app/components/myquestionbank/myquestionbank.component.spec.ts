import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyquestionbankComponent } from './myquestionbank.component';

describe('MyquestionbankComponent', () => {
  let component: MyquestionbankComponent;
  let fixture: ComponentFixture<MyquestionbankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyquestionbankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyquestionbankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
