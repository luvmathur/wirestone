import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';
import {AuthenticationService} from '../../services/authentication.service';



@Component({
  selector: 'app-addtopic',
  templateUrl: './addtopic.component.html',
  styleUrls: ['./addtopic.component.css']
})
export class AddtopicComponent implements OnInit {
public languages = new Array();

  constructor(private router:Router,
  private _cookieService:CookieService, private _headerservice: HeaderService,
  private _authenticationService: AuthenticationService) {
  }


getlanguages(){
this._authenticationService.getinformation().subscribe(data=>{
for (var value in data.Languages){
console.log(data.Languages[value]);
this.languages.push(data.Languages[value].language);
}
},

err => {
     alert('error');
}
);
}

addtopic(value:any){
this._authenticationService.addnewtopic(value).subscribe(data => {
      alert('Topic added successfully');

err => {
     alert('Topic not added');
}
});

}





  ngOnInit() {
this.getlanguages();

  }

}
