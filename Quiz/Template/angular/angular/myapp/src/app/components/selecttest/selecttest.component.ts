import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router'
import {CookieService} from 'angular2-cookie/core';
import {HeaderService} from '../../services/header.service';



@Component({
  selector: 'app-selecttest',
  templateUrl: './selecttest.component.html',
  styleUrls: ['./selecttest.component.css']

})
export class SelecttestComponent implements OnInit {

public languages = new Array();
public topics;
public languagequestions = new Array();


 constructor(private _authenticationService: AuthenticationService,
private router: Router,  private _cookieService:CookieService,
private _headerservice:HeaderService
){}


getlanguages(){
this._authenticationService.getinformation().subscribe(data=>{
for (var value in data.Languages){
this.languages.push(data.Languages[value].language);
}
},
err => {
     alert('error');
}
);
}


gettopics(value:any){
this._authenticationService.getlanguagetopics(value).subscribe(data=>{
for (var value in data){
this.topics = data.topics;
}
},
err => {
     alert('error');
}
);
}

starttest(value:any){
this._authenticationService.gettopicquestions(value).subscribe(data=>{
this.router.navigate(['testwindow', value.topic_id]);
},
err => {
     alert('error');
}
);
}


ngOnInit() {
this.getlanguages();
  }
}
