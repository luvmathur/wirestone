import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { CaroselComponent } from './components/carosel/carosel.component';
import {AuthenticationService} from './services/authentication.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import {HeaderService} from './services/header.service';
import { AddlanguageComponent } from './components/addlanguage/addlanguage.component';
import { AddtopicComponent } from './components/addtopic/addtopic.component';
import { MyquestionbankComponent } from './components/myquestionbank/myquestionbank.component';
import { UserdashboardComponent } from './components/userdashboard/userdashboard.component';
import { UserdetailComponent } from './components/userdetail/userdetail.component';
import { SelecttestComponent } from './components/selecttest/selecttest.component';
import { TestwindowComponent } from './components/testwindow/testwindow.component';
import { MyperformanceComponent } from './components/myperformance/myperformance.component';


const appRoutes : Routes = [
{path: 'register', component: RegisterComponent},
{path: 'testwindow/:id', component: TestwindowComponent},
{path: 'myperformance/:id', component: MyperformanceComponent},
 {path: 'home', component: HomePageComponent, children:[
        {path: '', component: AdminPanelComponent, outlet: 'childComponent'},
        {path: 'admin', component: AdminPanelComponent, outlet: 'childComponent'},
        {path: 'addlanguage', component: AddlanguageComponent, outlet: 'childComponent'},
         {path: 'addtopic', component: AddtopicComponent, outlet: 'childComponent'},
         {path: 'getquestions', component: MyquestionbankComponent, outlet: 'childComponent'}
        ]},

{path: 'userdashboard', component:UserdashboardComponent, children:[
        {path: 'userdetail', component: UserdetailComponent, outlet: 'childComponentdashboard'},
        {path: 'selecttest', component: SelecttestComponent, outlet: 'childComponentdashboard'},
        {path:'', redirectTo:'/userdashboard', pathMatch:'full'}
       ]},
{path: '', redirectTo : '/register', pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    NavbarComponent,
    AdminPanelComponent,
    HomePageComponent,
    CaroselComponent,
    AddlanguageComponent,
    AddtopicComponent,
    MyquestionbankComponent,
    UserdashboardComponent,
    UserdetailComponent,
    SelecttestComponent,
    TestwindowComponent,
    MyperformanceComponent,
  ],

  imports: [
    BrowserModule, FormsModule,ReactiveFormsModule, HttpModule, RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthenticationService, CookieService, HeaderService,TestwindowComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
