from Quiz.config  import db
from flask import Flask, jsonify, request, json, make_response
from flask_restful import reqparse
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
import datetime
from flask import Blueprint

topic_api = Blueprint('topic_api', __name__)



def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'access-token' in request.headers:
            token = request.headers['access-token']

        if not token:
            return jsonify({'Warning': 'Token Missing', 'status': '499'}), 499
        try:
            data = jwt.decode(token, '')
            current_user = data['id']
        except Exception as e:
            return jsonify({'Message': 'Token is invalid', 'status':'498'}), 498

        return f(current_user, *args, **kwargs)
    return decorated


@topic_api.route('/api/1.0/topic', methods=['POST'])
@token_required
def add_topic(current_user):
    topic = request.json['topic']
    language = request.json['language']
    level = request.json['level']
    db.mycursor.execute('select id from languages where language = "'+language+'"')
    language_id = db.mycursor.fetchone()
    db.mycursor.execute('select topic , language_id , level from topics where topic = "'+topic+'" and level= "'+level+'" and language_id = "'+str(language_id[0])+'"')
    topic_language_level = db.mycursor.fetchone()
    if language_id is not None:
         if topic_language_level is None:

                db.mycursor.execute('insert into topics(topic, language_id, level) values("'+topic+'", "'+str(language_id[0])+'", "'+level+'")')
                db.conn.commit()
                return jsonify({'message':{ 'topic':topic, 'language':language, 'level':level, 'status':'200'}})
         else:
            return jsonify({'error':{'message': 'topic with given language and level already exist'}})
    else:
        return jsonify({'error':{'message':'Add language first to database', 'status':''}})


