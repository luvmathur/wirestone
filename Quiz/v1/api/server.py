from Quiz.config  import db
from flask import Flask, render_template, jsonify, request, json, make_response
from flask_restful import reqparse, abort, Api, Resource
from flask_marshmallow import Marshmallow
import datetime
import jwt
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
from flask_wtf import Form
from wtforms import TextField, PasswordField, validators, Field
from functools import wraps
from Quiz.v1.api.questions import question_api
from Quiz.v1.api.users import user_api
from Quiz.v1.api.languages import language_api
from Quiz.v1.api.topics import topic_api


app = Flask(__name__)
app.register_blueprint(question_api)
app.register_blueprint(user_api)
app.register_blueprint(language_api)
app.register_blueprint(topic_api)

if __name__ == '__main__':
    app.run(debug=True)