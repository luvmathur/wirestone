from Quiz.config  import db
from flask import Flask, jsonify, request, json, make_response
from flask_restful import reqparse
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
import datetime
from flask import Blueprint

language_api = Blueprint('language_api', __name__)

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'access-token' in request.headers:
            token = request.headers['access-token']

        if not token:
            return jsonify({'Warning': 'Token Missing', 'status': '499'}), 499
        try:
            data = jwt.decode(token, '')
            current_user = data['id']
        except Exception as e:
            return jsonify({'Message': 'Token is invalid', 'status':'498'}), 498

        return f(current_user, *args, **kwargs)
    return decorated



@language_api.route('/api/1.0/language', methods = ['POST'])
@token_required
def add_language(current_user):
    language = request.json['language']
    db.mycursor.execute('select id from languages where language = "'+language+'"')
    language_id = db.mycursor.fetchone()

    if language_id == None:
            db.mycursor.execute('insert into languages(language) values("'+language.upper()+'")')
            db.conn.commit()
            return jsonify({'message':'language added successfully', 'status':'200'}), 201
    else:
        return jsonify({'error':{'message':'language already exist', 'status':'409'}}), 409


@language_api.route('/api/1.0/language', methods = ['GET'])
@token_required
def get_languages(current_user):
    db.mycursor.execute('select id , language from languages')
    languages = db.mycursor.fetchall()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()
    languages_list =[]
    for data in range(len(languages)):
        languages_list.append({'id': languages[data][0], 'language':languages[data][1]})
    if status != None:
        return jsonify ({'Languages':languages_list})
    else:
        return jsonify({'error':'token expire', 'status':'440'}), 440

@language_api.route('/api/1.0/language/<id>', methods = ['POST'])
@token_required
def update_language(current_user, id):
    language = request.json['language']
    db.mycursor.execute('select id from languages where id = "'+id+'"')
    language_id = db.mycursor.fetchone()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()
    if language_id == None:
        return jsonify({'error': {'message': 'language id do not exist', 'status': '404'}}), 404

    else:
        if status != None:
            db.mycursor.execute('select count(language) from languages where language = "'+language+'"')
            count = db.mycursor.fetchone()
            if str(count[0]) == str(0):
                    db.mycursor.execute('update languages set language = "'+language+'" where id = "'+id+'"' )
                    db.conn.commit()
                    return jsonify({'message': 'language updated successfully' , 'status':'200'}), 200
            else:
                return jsonify({'error':{'message':'language already exist', 'status':'409'}}), 409

        else:
            return jsonify({'error': 'token expire', 'status': '440'}), 440