from Quiz.config import db
from flask import Flask, jsonify, request, json
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
import datetime
from flask import Blueprint

question_api = Blueprint('question_api', __name__)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'access-token' in request.headers:
            token = request.headers['access-token']

        if not token:
            return jsonify({'Warning': 'Token Missing', 'status': '499'}), 499
        try:
            data = jwt.decode(token, '')
            current_user = data['id']
        except Exception :
            return jsonify({'Message': 'Token is invalid', 'status': '498'}), 498

        return f(current_user, *args, **kwargs)
    return decorated


@question_api.route('/api/1.0/questions', methods=['POST'])
@token_required
def add_question(current_user):
    """
        Description.
        add_question adds the multiple choice question to a database
    """

    question = request.json['question']
    choice1 = request.json['choice1']
    choice2 = request.json['choice2']
    choice3 = request.json['choice3']
    choice4 = request.json['choice4']
    answer = request.json['answer']
    language = request.json['language']
    topic = request.json['topic']
    db.mycursor.execute('select id from languages where language = "'+language+'"')
    language_id = db.mycursor.fetchone()
    db.mycursor.execute('select id  from topics where topic = "'+topic+'" ')
    topic_id = db.mycursor.fetchone()
    db.mycursor.execute('select status from tokens where user_id = "' + str(current_user) + '"')
    status = db.mycursor.fetchone()
    if status is not None:
        if language_id is not None:
            if topic_id is not None:
                if question and choice1 and choice2 and choice3 and choice4 and answer and language and topic is not None:
                    db.mycursor.execute('insert into questions(question, choice1, choice2, choice3, choice4, answer, language_id, topic_id)values("'+question+'", "'+choice1+'", "'+choice2+'", "'+choice3+'", "'+choice4+'", "'+answer+'", "'+str(language_id[0])+'", "'+str(topic_id[0])+'")')
                    db.conn.commit()
                    return jsonify({'message': 'question added successfully', 'status':'200'}), 200

                else:
                    return jsonify({'Message': 'All fields required', 'status':'412'}), 412
            else:
                return jsonify({'error':{'message':'topic does not exist'}})

        else:
            return jsonify({'error':{'message':'add this language to database'}})

    else:
        return jsonify({'error':{'Message': 'Token expire', 'status':'406'}}), 406


@question_api.route('/api/1.0/questions', methods=['GET'])
@token_required
def get_questions(current_user):
    """
        Description.
        get_questions returns all questions of various languages and topics
    """

    db.mycursor.execute('select id, question, choice1, choice2, choice3, choice4, answer from questions')
    questions_data = db.mycursor.fetchall()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()
    question_list = []
    for question in range(len(questions_data)):
        question_list.append({'id': questions_data[question][0], 'question': questions_data[question][1],
                              'choice1': questions_data[question][2],
                              'choice2': questions_data[question][3],
                              'choice3': questions_data[question][4],
                              'choice4': questions_data[question][5],
                              'answer': questions_data[question][6]})

    if status is not None:
        return jsonify({'questions': question_list}), 200
    else:
        return jsonify({'Message': 'Token expire'}), 400


@question_api.route('/api/1.0/question/update/<id>', methods=['POST'])
@token_required
def update_question(current_user, id):
    """
        Description.
        update_questions update the  question when user provides question id
    """

    db.mycursor.execute('select id from questions where id ="'+id+'"')
    question_id = db.mycursor.fetchone()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()

    question = request.json['question']
    choice1 = request.json['choice1']
    choice2 = request.json['choice2']
    choice3 = request.json['choice3']
    choice4 = request.json['choice4']
    answer = request.json['answer']

    if question_id is None:
        return jsonify({'Message': 'provided question id do not exist'}), 404

    else:
        if status is not None:
            if question and choice1 and choice2 and choice3 and choice4 and answer is not None:
                db.mycursor.execute('update questions set question = "' + question + '", choice1="' + choice1 + '", choice2="' + choice2 + '", choice3="' + choice3 + '", choice4="' + choice4 + '", answer="' + answer + '" where id = "' + str(id) + '" ')
                db.conn.commit()
                return jsonify({'Message': 'Question updated successfully'}), 200
            else:
                return jsonify({'Message': 'All fields are required'}), 400

        else:
            return jsonify({'Message': 'Token expire'}), 440


@question_api.route('/api/1.0/question/<id>', methods=['DELETE'])
@token_required
def delete_question(current_user, id):
    """
        Description.
        delete_question delete the  question when user provides question id
    """

    db.mycursor.execute('select id from questions where id ="' + id + '"')
    question_id = db.mycursor.fetchone()
    db.mycursor.execute('select status from tokens where user_id ="' + str(current_user) + '"')
    status = db.mycursor.fetchone()

    if question_id is not None:
        if status is not None:
            db.mycursor.execute('delete from questions where id ="' + str(id) + '" ')
            db.conn.commit()
            return jsonify({'Message': 'Requested question id deleted', 'status': '200'}), 200
        else:
            return jsonify({'error': {'Message': 'Token expire', 'status': '440'}}), 440

    else:
        return jsonify({'error': {'Message': 'quetion id do not exist', 'status': '404'}}), 404
